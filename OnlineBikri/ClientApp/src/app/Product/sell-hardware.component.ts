import { parseSelectorToR3Selector } from '@angular/compiler/src/core';
import { Component } from '@angular/core';
import { FormArray } from '@angular/forms';
import { isNumber } from 'util';

@Component({
  selector: 'app-root',
  templateUrl: './sell-hardware.component.html',
  //styleUrls: ['./app.component.css']
})
  
export class SellHardwareComponent {
  title = 'custom-search-filter-example';
  //isShow : boolean = false;
  searchedKeyword: string;
  selectedItem: Item;
  total: number = 0.00;
  isShow: boolean = true;
  selectedItemList:Array<Item>=[];
  itemList: Array<Item> = [
    {ID:1, name: "Berger Paints Luxol Paint", color: "white", Quantiy: 1, unit: "Kg", price: 20.00 },
    { ID:2, name: "Asian Paints Luxol Paint", color: "Red", Quantiy: 5, unit: "Kg", price: 10.00 },
    { ID:3, name: "Putty", color: "Blue", Quantiy: 10, unit: "ltr", price: 200.00 },
    { ID:4, name: "Brush House Wall paint brush", color: "white", Quantiy: 1, unit: "pcs", price: 100.00 },
    { ID:5, name: "Berger Paints Luxol Paint other", color: "purple", Quantiy: 1, unit: "Kg", price: 70.00 },
    { ID:6, name: "Tarpine", color: "white", Quantiy: 1, unit: "ltr", price: 50.00 },
    { ID:7,name: "Asian Paints double", color: "Green", Quantiy: 1, unit: "ltr", price: 100.00 },
    { ID:8,name: "Brush big size", color: "white", Quantiy: 1, unit: "pcs", price: 80.00 },
    { ID:9, name: "Paint roller", color: "red", Quantiy: 1, unit: "pcs", price: 110.00 },
  ];
    myForm: any;
  
  qty: number = 0;

  hide() {
    console.log("is to Hide: "+this.isShow);
  }

  KeyPress(event: any) {
    //console.log(event.target.value + ' = ' + this.selectedItem.Quantiy);
    //if (isNumber(parseInt(event.target.value))) {
    //  let obj = Object.assign({}, this.selectedItem);
    //  obj.Quantiy = parseInt(event.target.value);
    //  console.log(obj.Quantiy);
    //  this.CalclulateTotal(obj);
    //}
    
    //this.isShow =  this.searchedKeyword.length > 0 ? true : false;
  }

  CalclulateTotal(qty: any, price: number) {
    this.total = 0.00;
    for (let i = 0; i < this.selectedItemList.length; i++) {
     if(isNumber(parseInt(qty)))
      this.total = this.total + (this.selectedItemList[i].Quantiy * this.selectedItemList[i].price);
    }

    //if (isNumber(parseInt(qty))) {
      
    //  //this.total = this.total + (qty * price);
    //  this.selectedItemList.forEach(function (itm: Item, index: number) {
    //    console.log(itm.Quantiy+","+itm.price + ", total="+this.to);
    //    //this.total = this.total + (itm.Quantiy * itm.price);
    //  });
    //}

    console.log("total:"+this.total);
    
  }

  RemoveItem(selectedItem: Item) {
    selectedItem.Quantiy--;
    if (selectedItem.Quantiy == 0)
    this.selectedItemList.forEach((itm, index) => {
      if (itm.ID == selectedItem.ID) this.selectedItemList.splice(index, 1);
    });
    this.CalclulateTotal(selectedItem.Quantiy - (selectedItem.Quantiy * 2), selectedItem.price - (selectedItem.price*2) );
  }

  AddItem(selectedItem: Item) {
    //this.selectedItem.name = itemName;
    if (!this.selectedItemList.some(itm => itm.ID == selectedItem.ID)) {
      this.selectedItemList.push(selectedItem);
    }
    else {
      this.selectedItemList.find(itm => itm.ID == selectedItem.ID).Quantiy++;
    }
    
    // this.total = this.total + (selectedItem.price * selectedItem.Quantiy);
    
    this.CalclulateTotal(selectedItem.Quantiy, selectedItem.price);
    
    //console.log(this.total);
  }

  

  hardwareDataSet = [
    {ID:1, name: 'Berger Paints Luxol Paint', color: 'White', weight: '1ltr', size:null },
    {ID:2, name: 'Berger Paints High Gloss Enamel Paint', color: 'red', weight: '1ltr', size: null },
    {ID:3, name: 'Brush House Wall paint brush', color: 'red', weight: '1ltr', size: '2 inhes' },
    { ID:4,name: 'Nail', color: 'black', weight: '1kg', size: '1.5 inhes' },
    {ID:5, name: 'putty', color: 'white', weight: '5 kg', size: null},
  ];
 
}

class Item {
  ID: number;
  name: string;
  Quantiy: number;
  price: number;
  color: string;
  unit: string;

}
