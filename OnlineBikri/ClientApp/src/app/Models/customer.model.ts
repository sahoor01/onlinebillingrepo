
export class Customer {
  firstName: string;
  lastName: string;
  mobileNumber: string;
  address: string;
}
