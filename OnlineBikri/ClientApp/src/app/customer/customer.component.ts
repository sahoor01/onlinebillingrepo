import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Customer } from '../Models/customer.model';
import { CustomerService } from '../Services/customer.service'


@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html'
})
export class CustomerComponent implements OnInit {
  title = 'Cusomter';
  public customer: Customer;// = { firstName: 'abc', lastName: 'xyz', address: 'BBSR', mobileNumber: '8008337857' };
  public mobileNumber: string;
  constructor(private _router: Router, private _CustomerService: CustomerService,private _activatedRoute:ActivatedRoute ) {
    //this.customer = { FirstName: 'abc', LastName: 'xyz', Address: 'BBSR', MobileNumber: '8008337857' }
    this.mobileNumber = this._activatedRoute.snapshot.params['MobileNumber'];
    _CustomerService.GetDetail(this.mobileNumber).subscribe((data: Customer) => {
      console.log(data);
      this.customer = data;
    }, error => { console.error(error) });
  }
    ngOnInit(): void {
      this.mobileNumber = this._activatedRoute.snapshot.params['MobileNumber'];
      console.log(this.mobileNumber);
  }

  OnBackButtonClick(): void{
    this._router.navigate(['/customers']);
  }
}
