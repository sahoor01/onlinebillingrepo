import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient  } from '@angular/common/http';

import { Router } from '@angular/router';
import { Customer } from '../Models/customer.model';
import { CustomerService } from '../Services/customer.service'

@Component({
  selector: 'app-customer',
  templateUrl: './customerlist.component.html'
})
export class CustomerListComponent implements OnInit {
  title = 'Cusomter List screen';
  public selectedCustomer: Customer;
  public customerlist: Customer[];
  
  private accessPointUrl: string = 'https://localhost:5001/api/Customer';
  //[
  //  { FirstName: 'abc', LastName: 'xyz', Address: 'BBSR', MobileNumber: '8008337857' },
  //  { FirstName: 'Hello', LastName: 'world', Address: 'Puri', MobileNumber: '8374432047' }
  //];
  
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private _router: Router, private _CustomerService: CustomerService) {
    //console.log('BASE URL IS:-->  '+baseUrl + 'api/customer');
    //_CustomerService.get().subscribe((data: Customer[]) => this.customerlist = data);
    _CustomerService.get().subscribe((data: Customer[]) => {
      console.log(data);
      this.customerlist = data;
    }, error => { console.error(error) });
    //http.get<Customer[]>(baseUrl + 'api/customer').subscribe(result => {
    //  console.log(result);
    //}, error => console.error('error coccured at-'+ error ));
  }
  ngOnInit(): void {
   
    
  }

  RowSelected(customer: Customer) {
    this.selectedCustomer = customer;
    this._router.navigate(['/customerDetail', customer.mobileNumber]);
  }

  
}
