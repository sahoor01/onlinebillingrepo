﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineBikriDataAPI.Models;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineBikriDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    public class CustomerController : ControllerBase
    {
        public List<Customer> customerlist = new List<Customer>();

        // GET: api/<CustomerController>
        [HttpGet]
        public IEnumerable<Customer> Get()
        {
            customerlist.Add(new Customer() { FirstName = "abc", LastName = "xyz", Address = "BBSR", MobileNumber = "8008337857" });
            customerlist.Add(new Customer() { FirstName = "abc1", LastName = "xyz1", Address = "BBSR1", MobileNumber = "8374432047" });

            return customerlist;
        }

       // GET api/<CustomerController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        //[HttpGet("{mobileNumber}")]
        //public Customer Get(string mobileNumber)
        //{
        //    return new Customer() { FirstName = "abc1", LastName = "xyz1", Address = "BBSR1", MobileNumber = "8374432047" };
        //}

        // GET api/<CustomerController>/"8008337857"
        [HttpGet("{mobileNumber}")]
        public Customer Get(string mobileNumber)
        {
            //return (Customer)customerlist.Where(a => a.MobileNumber == mobileNumber);
            return new Customer();
        }

        // POST api/<CustomerController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<CustomerController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<CustomerController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
